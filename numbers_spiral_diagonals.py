import time
import math
start_time = time.time()

def numbers_spiral_diagonals(size):
    sum_diagonal = 1
    last_num = 0
    for i in range(1,int(math.floor(size/2))+1):
        for j in range(1,5):
            sum_diagonal += last_num + 1 + i*2*j
        last_num +=  i*2*j

    return sum_diagonal

print(numbers_spiral_diagonals(1001))
print(f"--- {(time.time() - start_time):.10f} seconds ---"  )